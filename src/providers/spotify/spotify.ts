import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the SpotifyProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SpotifyProvider {

  constructor(public http: Http) {
    console.log('Hello SpotifyProvider Provider');
  }

  getDatos(){
    // return this.http.get('https://api.myjson.com/bins/w076v')
    return this.http.get('assets/spotify.json')
      .map( res => res.json() )
      .map((data: any) => {
        var size = 50;
        return data.slice(0, size);
      });
  }

}
