import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { InicioPage } from "../pages/inicio/inicio";
import { PerfilesPage } from '../pages/perfiles/perfiles';
import { ContactoPage } from '../pages/contacto/contacto';
import { AcercaPage } from '../pages/acerca/acerca';
import { YoutubePage } from '../pages/youtube/youtube';
import { SpotifyPage } from '../pages/spotify/spotify';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('NAV') nav: Nav;
  public rootPage: any;
  public pages: Array<{ titulo: string, component: any, icon: string }>;

  constructor(
    platform:     Platform,
    statusBar:    StatusBar,
    splashScreen: SplashScreen
  ) {

    this.rootPage = InicioPage;
    this.pages = [
      { titulo: 'Inicio',          component: InicioPage,   icon: 'home'},
      { titulo: 'Perfiles Deezer', component: PerfilesPage, icon: 'person'},
      { titulo: 'Youtube',         component: YoutubePage,  icon: 'logo-youtube'},
      //{ titulo: 'Spotify',         component: SpotifyPage,  icon: '../assets/images/spotify-24.png'},
      { titulo: 'Contacto',         component: ContactoPage,  icon: 'mail'},
      { titulo: 'Acerca de',       component: AcercaPage,   icon: 'information-circle'}
    ];

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  goToPage(page){
    this.nav.setRoot(page);
  }

  goToInicio() {
    this.nav.setRoot(InicioPage);
  }

  goToPerfilesDeezer() {
    this.nav.setRoot(PerfilesPage);
  }

  goToYoutube() {
    this.nav.setRoot(YoutubePage);
  }

  goToSpotify() {
    this.nav.setRoot(SpotifyPage);
  }

  goToContacto() {
    this.nav.setRoot(ContactoPage);
  }

  goToAcerca() {
    this.nav.setRoot(AcercaPage);
  }

}
