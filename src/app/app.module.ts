import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';

import { InicioPage } from '../pages/inicio/inicio';
import { PerfilesPage } from '../pages/perfiles/perfiles';
import { PerfilDetallePage } from '../pages/perfil-detalle/perfil-detalle';

import { ContactoPage } from '../pages/contacto/contacto';
import { AcercaPage } from '../pages/acerca/acerca';

import { PlaylistsPage } from '../pages/playlists/playlists';
import { CancionesPage } from '../pages/canciones/canciones';
import { CancionPopoverPage } from '../pages/cancion-popover/cancion-popover'

import { TecnoPlayerComponent } from '../components/tecno-player/tecno-player';
import { YtProvider } from '../providers/yt/yt';

import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { YoutubePage } from '../pages/youtube/youtube';

import { SpotifyPage } from '../pages/spotify/spotify';
import { SpotifyProvider } from '../providers/spotify/spotify';

@NgModule({
  declarations: [
    MyApp,

    InicioPage,
    PerfilesPage,
    PerfilDetallePage,
    ContactoPage,
    AcercaPage,

    PlaylistsPage,
    CancionesPage,
    CancionPopoverPage,
    
    YoutubePage,

    TecnoPlayerComponent,

    SpotifyPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    
    InicioPage,
    PerfilesPage,
    PerfilDetallePage,
    ContactoPage,
    AcercaPage,

    PlaylistsPage,
    CancionesPage,
    CancionPopoverPage,

    YoutubePage,

    TecnoPlayerComponent,

    SpotifyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }, 
    YtProvider,
    YoutubeVideoPlayer, SpotifyProvider
  ]
})
export class AppModule {}
