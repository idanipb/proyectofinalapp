import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpotifyProvider } from '../../providers/spotify/spotify'
import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';


/**
 * Generated class for the SpotifyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-spotify',
  templateUrl: 'spotify.html',
})
export class SpotifyPage {

  canciones: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public SpProvider: SpotifyProvider, public http: Http) {
  }

  ionViewDidLoad() {
    
    this.SpProvider.getDatos().subscribe((data) => {
      this.canciones = data;
    });
  }

  openSong(url) {
    window.open(url);
  }
  

}
